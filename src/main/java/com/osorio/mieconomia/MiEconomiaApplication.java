package com.osorio.mieconomia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiEconomiaApplication {
   public static void main(String[] args) {
        SpringApplication.run(MiEconomiaApplication.class, args);
    }

}
